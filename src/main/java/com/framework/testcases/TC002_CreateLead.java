package com.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;

public class TC002_CreateLead extends ProjectMethods {
	
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "Create a new Lead in leaftaps";
		testNodes = "Leads";
		author = "Dinesh";
		category = "Smoke";
		dataSheetName = "TC2";
	}

		
	@Test(dataProvider= "fetchData")
	public void createlead(String fname , String lname ,String cname) {
		new LoginPage()
		.enterUsername("DemoCSR")
		.enterPassword("crmsfa")
		.clickLogin();
		
		new HomePage()
			.clickLinkCrmSfa()
			.clickLeads()
			.clickCreateLeads()
			.enterFirstName(fname)
			.enterLastName(lname)
			.enterCompanyName(cname)
			.clickCreateLeadButton()
			.verifyFirstName(fname);
			
	}
	

}
