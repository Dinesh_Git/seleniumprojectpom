package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods {
	
	
	@BeforeTest
	public void setData() {
		
		testCaseName = "TC001_LoginLogout";
		testDescription = "Login and Logout leaftaps";
		testNodes = "Leads";
		author = "Dinesh";
		category = "Smoke";
		dataSheetName = "TC1";
	}

	
	@Test(dataProvider= "fetchData")
	public void login(String username , String password) {
		
		/*LoginPage lp = new LoginPage();
		lp.enterUsername("");
		lp.enterPassword("");
		*/
		
		new LoginPage()
			.enterUsername(username)
			.enterPassword(password)
			.clickLogin()
			.clickLogout();
		
	}
	

}
