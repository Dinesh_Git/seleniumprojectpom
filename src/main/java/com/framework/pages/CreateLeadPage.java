package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH,using="(//input[@name='firstName'])[3]") WebElement elefirstname;
	@FindBy(how = How.XPATH,using="(//input[@name='lastName'])[3]") WebElement elelastname;
	@FindBy(how = How.XPATH,using="(//input[@name='companyName'])[2]") WebElement elecompanyname;
	@FindBy(how = How.XPATH,using="//input[@class='smallSubmit']") WebElement elecreateleadbutton;
	
	
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(elefirstname, data);
		return this;
	}
	
	public CreateLeadPage enterLastName(String data) {
		clearAndType(elelastname, data);
		return this;
	}
	
	public CreateLeadPage enterCompanyName(String data) {
		clearAndType(elecompanyname, data);
		return this;
	}
	
	public ViewLeadPage clickCreateLeadButton() {
		click(elecreateleadbutton);
		return new ViewLeadPage();
		
	}
	
	
	
	

}
