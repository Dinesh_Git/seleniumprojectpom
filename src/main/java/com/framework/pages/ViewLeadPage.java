package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	
	public ViewLeadPage() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="viewLead_firstName_sp") WebElement eleverifirstname;
	
	public ViewLeadPage verifyFirstName(String data) {
		verifyExactText(eleverifirstname, data);
		return this;
		
	}
	

}
